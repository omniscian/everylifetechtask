//
//  EveryLifeTechTaskTests.swift
//  EveryLifeTechTaskTests
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//

import XCTest
@testable import EveryLifeTechTask

class CareTaskListViewModelTests: XCTestCase {
    
    var SUT = CareTaskListViewModel(service: MockCareTaskServiceImpl())

    func testFetchCareTaskListSetsCareTaskList() {
        SUT.fetchCareTaskList()
        XCTAssertEqual(SUT.careTaskList.count, 6)
    }
    
    func testFilterTogglesShouldShowType() {
        SUT.fetchCareTaskList()
        
        SUT.filter(type: "general")
        XCTAssertEqual(SUT.shouldShowType(type: "general"), false)
        SUT.filter(type: "general")
        XCTAssertEqual(SUT.shouldShowType(type: "general"), true)
        
        SUT.filter(type: "medication")
        XCTAssertEqual(SUT.shouldShowType(type: "medication"), false)
        SUT.filter(type: "medication")
        XCTAssertEqual(SUT.shouldShowType(type: "medication"), true)
        
        SUT.filter(type: "hydration")
        XCTAssertEqual(SUT.shouldShowType(type: "hydration"), false)
        SUT.filter(type: "hydration")
        XCTAssertEqual(SUT.shouldShowType(type: "hydration"), true)
        
        SUT.filter(type: "nutrition")
        XCTAssertEqual(SUT.shouldShowType(type: "nutrition"), false)
        SUT.filter(type: "nutrition")
        XCTAssertEqual(SUT.shouldShowType(type: "nutrition"), true)
    }
}
