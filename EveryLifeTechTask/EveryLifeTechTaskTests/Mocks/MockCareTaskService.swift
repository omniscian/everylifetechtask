//
//  MockCareTaskService.swift
//  EveryLifeTechTaskTests
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//

import Foundation
import Combine
@testable import EveryLifeTechTask

class MockCareTaskServiceImpl: CareTaskService {
    func fetchTasks() -> AnyPublisher<[CareTaskListModel], Error> {
        let array: [CareTaskListModel] = Bundle.main.decode([CareTaskListModel].self, from: "data.json")
        return Just<[CareTaskListModel]>(array)
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
    }
}
