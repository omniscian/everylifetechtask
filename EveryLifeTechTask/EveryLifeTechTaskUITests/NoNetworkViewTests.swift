//
//  EveryLifeTechTaskUITests.swift
//  EveryLifeTechTaskUITests
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//

import XCTest

class NoNetworkViewTests: XCTestCase {

    private var app: XCUIApplication!
    
    override func setUpWithError() throws {
        super.setUp()
        self.app = XCUIApplication()
        app.launchArguments.append("-networkConnected")
        app.launchArguments.append("NO")
        self.app.launch()
    }
    
    func testNavigationBarTitleIsSet() {
        let list = self.app.navigationBars["Tasks"]
        XCTAssert(list.exists)
    }
    
    func testNoNetworkTitleIsShown() {
        let networkTitle = self.app.staticTexts["NoNetworkTitle"]
        XCTAssert(networkTitle.exists)
        XCTAssertEqual(networkTitle.label, "No network connection")
    }

    func testNoNetworkDescriptionIsShown() {
        let networkTitle = self.app.staticTexts["NoNetworkDesc"]
        XCTAssert(networkTitle.exists)
        XCTAssertEqual(networkTitle.label, "This data may be outdated. Please reconnect and try again.")
    }
    
    func testListIsShown() {
        let list = self.app.tables["taskList"]
        XCTAssert(list.exists)
        XCTAssertEqual(list.images.count, 6)
    }
    
    func testFilterButtonsAreShown() {
        let buttons = self.app.buttons
        XCTAssertEqual(buttons.count, 4)
    }
}
