//
//  CareTaskListViewModel.swift
//  EveryLifeTechTask
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//

import Foundation
import Combine
import CoreData

/**
    A view model class responsible for fetching and preparing the data to show on the care task list view
*/

class CareTaskListViewModel: ObservableObject {
    private let careTaskService: CareTaskService
    
    /// CareTaskList is @Published to automatically trigger UI changes to views that are observing.
    @Published var careTaskList = [CareTaskListModel]()
    private var fullTaskList = [CareTaskListModel]()
    
    private var cancellable: AnyCancellable?
    private var showGeneral = true
    private var showMedication = true
    private var showHydration = true
    private var showNutrition = true
    
    /// Service is injected for test purposes
    init(service: CareTaskService) {
        self.careTaskService = service
    }
    
    func fetchCareTaskList() {
        cancellable = self.careTaskService.fetchTasks().sink(receiveCompletion: { error in
            if !UserDefaults.standard.bool(forKey: "isRunningTests") {
                self.fetchEntries()
            }
        }, receiveValue: { container in
            if UserDefaults.standard.bool(forKey: "isRunningTests") {
                self.careTaskList = container
                self.fullTaskList = container
            } else {
                for careTask in container {
                    let context = PersistenceController.shared.container.viewContext
                    let task = CareTask(context: context)
                    task.id = Int16(careTask.id)
                    task.name = careTask.name
                    task.desc = careTask.desc
                    task.type = careTask.type
                }
                
                PersistenceController.shared.saveContext()
                // fetch from core data
                self.fetchEntries()
            }
        })
    }
    
    // Refactor: Setup a PersistenceController mock and inject to make fetch entries testable
    private func fetchEntries() {
        let request : NSFetchRequest<CareTask> = CareTask.fetchRequest()
        do {
            let context = PersistenceController.shared.container.viewContext
            let entriesCoreData = try context.fetch(request)
            self.careTaskList = entriesCoreData.map {
                CareTaskListModel.init(careTask: $0)
            }.sorted(by: { $0.id < $1.id })
            
            self.fullTaskList = self.careTaskList
        } catch {
            self.careTaskList = []
        }
    }
    
    func shouldShowType(type: String) -> Bool {
        switch type {
        case "general":
            return self.showGeneral
        case "medication":
            return self.showMedication
        case "hydration":
            return self.showHydration
        case "nutrition":
            return self.showNutrition
        default:
            return false
        }
    }
    
    func filter(type: String) {
        switch type {
        case "general":
            self.showGeneral.toggle()
        case "medication":
            self.showMedication.toggle()
        case "hydration":
            self.showHydration.toggle()
        case "nutrition":
            self.showNutrition.toggle()
        default:
            break
        }
        
        self.careTaskList = self.fullTaskList
    }
}
