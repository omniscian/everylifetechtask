//
//  CareTaskListModel.swift
//  EveryLifeTechTask
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//

import Foundation

/**
    A model struct representing the care task json data that comes back from the web serivce
*/

struct CareTaskListModel: Decodable, Hashable {
    let id: Int
    let name: String
    let desc: String
    let type: String
    
    /// Use coding keys since description is already defined 
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case desc = "description"
        case type
    }
    
    /// Image names have been stored with the same name as type, so just return that.
    var imageName: String {
        return self.type
    }
    
    init(careTask: CareTask) {
        self.id = Int(careTask.id)
        self.name = careTask.name ?? ""
        self.desc = careTask.desc ?? ""
        self.type = careTask.type ?? ""
    }
}
