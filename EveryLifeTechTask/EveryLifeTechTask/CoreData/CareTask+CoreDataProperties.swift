//
//  CareTask+CoreDataProperties.swift
//  EveryLifeTechTask
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//
//

import Foundation
import CoreData

extension CareTask {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CareTask> {
        return NSFetchRequest<CareTask>(entityName: "CareTask")
    }

    @NSManaged public var desc: String?
    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var type: String?

}

extension CareTask : Identifiable {

}
