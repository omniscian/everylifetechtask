//
//  NetworkMonitor.swift
//  EveryLifeTechTask
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//

import Foundation
import Network
import Combine

/**
    A wrapper class around NWPathMonitor for monitoring changes to the network.
 
    When updated, a @published Boolean value is updated triggering changes to its observers.
    An example is the CareTaskList view which shows a "No network message" when connection is lost.
*/

final class NetworkMonitor: ObservableObject {
    
    private let monitor = NWPathMonitor()
    private let queue = DispatchQueue(label: "networkMonitor")
    
    @Published var isConnected = true
    
    // Refactor isConnected into protocol to remove argument check below
    // Not done yet due to isConnected being @Published and wrappers are not supported in protocols
    init() {
        if ProcessInfo.processInfo.arguments.contains("-networkConnected") {
            self.isConnected = UserDefaults.standard.bool(forKey: "networkConnected")
        } else {
            monitor.pathUpdateHandler = { [weak self] path in
                DispatchQueue.main.async {
                    self?.isConnected = path.status == .satisfied
                }
            }
            monitor.start(queue: self.queue)
        }
    }
}


