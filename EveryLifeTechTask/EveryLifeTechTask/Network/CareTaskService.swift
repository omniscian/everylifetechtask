//
//  CareTaskService.swift
//  EveryLifeTechTask
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//

import Foundation
import Combine

/**
    A class which fetches a list of care tasks from the given URL
 
    - Throws:
        - URLError.badUrl if url cannot be created
    - Returns:
        - An array of CareTaskListModel
*/

/// Protocol used for dependancy injection to make class testable

protocol CareTaskService {
    func fetchTasks() -> AnyPublisher<[CareTaskListModel], Error>
}

class CareTaskServiceImpl: CareTaskService {
    private let url = "https://adam-deleteme.s3.amazonaws.com/tasks.json"
    
    func fetchTasks() -> AnyPublisher<[CareTaskListModel], Error> {
        guard let url = URL(string: self.url) else {
            let error = URLError(.badURL)
            return Fail(error: error).eraseToAnyPublisher()
        }
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map{ $0.data }
            .decode(type: [CareTaskListModel].self, decoder: JSONDecoder())
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
}
