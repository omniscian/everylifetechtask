//
//  EveryLifeTechTaskApp.swift
//  EveryLifeTechTask
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//

import SwiftUI

@main
struct EveryLifeTechTaskApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            CareTaskList(networkMonitor: NetworkMonitor(),
                         viewModel: CareTaskListViewModel(service: CareTaskServiceImpl()))
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
