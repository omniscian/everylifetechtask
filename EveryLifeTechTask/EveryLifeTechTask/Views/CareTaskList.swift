//
//  ContentView.swift
//  EveryLifeTechTask
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//

import SwiftUI
import CoreData

struct CareTaskList: View {
    @ObservedObject private var viewModel: CareTaskListViewModel
    @ObservedObject private var networkMonitor: NetworkMonitor
    
    /// A custom initialiser for injecting the view model and network monitor.
    init(networkMonitor: NetworkMonitor, viewModel: CareTaskListViewModel) {
        self.viewModel = viewModel
        self.networkMonitor = networkMonitor
        UITableView.appearance().contentInset.top = -35
    }
    
    var body: some View {
        NavigationView {
            VStack(spacing: 0) {
                if !self.networkMonitor.isConnected {
                    NoNetworkView()
                        .transition(.slide)
                        .animation(.easeIn)
                }
                if viewModel.careTaskList.isEmpty {
                    ProgressView()
                } else {
                    List {
                        ForEach(viewModel.careTaskList, id: \.id) { careTask in
                            if viewModel.shouldShowType(type: careTask.type) {
                                CareTaskCell(careTask: careTask)
                            }
                        }
                    }
                    .accessibility(identifier: "taskList")
                    .padding(EdgeInsets(top: 0, leading: -20, bottom: 0, trailing: -20))
                    .transition(.slide)
                    .animation(.easeIn)
                    FilterToolbar { type in
                        self.viewModel.filter(type: type)
                    }
                }
            }
            .navigationTitle("Tasks")
            .navigationBarTitleDisplayMode(.inline)
            .onAppear {
                self.viewModel.fetchCareTaskList()
            }
        }
        .accessibility(identifier: "navBarTitle")
    }
}

struct CareTaskList_Previews: PreviewProvider {
    static var previews: some View {
        CareTaskList(networkMonitor: NetworkMonitor(), viewModel: CareTaskListViewModel(service: CareTaskServiceImpl())).environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
