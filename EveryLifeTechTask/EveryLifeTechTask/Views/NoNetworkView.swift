//
//  NoNetworkView.swift
//  EveryLifeTechTask
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//

import SwiftUI

struct NoNetworkView: View {
    var body: some View {
        ZStack {
            Color.red
            .edgesIgnoringSafeArea(.all)
            VStack (spacing: 5) {
                Text("No network connection")
                    .font(.system(size: 28)).bold()
                    .multilineTextAlignment(.center)
                    .accessibility(identifier: "NoNetworkTitle")
                Text("This data may be outdated. Please reconnect and try again.")
                    .font(.system(size: 16))
                    .multilineTextAlignment(.center)
                    .lineLimit(2)
                    .accessibility(identifier: "NoNetworkDesc")
            }
        }.frame(width: .infinity, height: 100, alignment: .center)
    }
}
