//
//  FilterButton.swift
//  EveryLifeTechTask
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//

import SwiftUI

struct FilterButton: View {
    var type: String
    var onPressFunction: (_ type: String) -> Void
    
    var body: some View {
        Button {
            self.onPressFunction(type)
        } label: {
            Image(type)
        }.frame(width: 60, height: 60, alignment: .center)
    }
}
