//
//  FilterToolbar.swift
//  EveryLifeTechTask
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//

import SwiftUI

struct FilterToolbar: View {
    var onPressFunction: (_ type: String) -> Void
    
    var body: some View {
        HStack {
            FilterButton(type: "general", onPressFunction: self.onPressFunction)
            FilterButton(type: "medication", onPressFunction: self.onPressFunction)
            FilterButton(type: "hydration", onPressFunction: self.onPressFunction)
            FilterButton(type: "nutrition", onPressFunction: self.onPressFunction)
        }
    }
}
