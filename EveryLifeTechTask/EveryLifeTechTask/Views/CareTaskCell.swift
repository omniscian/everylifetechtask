//
//  CareTaskCell.swift
//  EveryLifeTechTask
//
//  Created by Ian Layland-Houghton on 23/05/2021.
//

import SwiftUI

struct CareTaskCell: View {
    var careTask: CareTaskListModel
    
    var body: some View {
        HStack(alignment: .top) {
            Image(careTask.imageName)
                .padding(.vertical, 6)
            VStack(alignment: .leading, spacing: 5) {
                Text(careTask.name)
                    .font(.system(size: 28))
                Text(careTask.desc)
                Spacer()
            }
        }
    }
}

