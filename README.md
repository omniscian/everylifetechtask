# IAN LAYLAND-HOUGHTON EVERY LIFE TECH TASK README #

This technical task is responsible for getting dummy "care task" data from a web service and displaying it to a matched designed, using SwiftUI and Combine. 

### Summary ###

I really enjoyed this tech task due to it being my first real project using SwiftUI and Combine. 
The code is written in MVVM. The app makes a network request to retrieve the care task on app launch, parse the response and saves it into a Core Data database and displays that list to the user.
If the user is offline, a banner is shown at the top of the list displaying a no network connection title and a message that the data may be outdated. Any previous data stored in the database will be shown to give the user an offline experience.
I have used dependancy injection where possible to promote use of clean architecture and testable code.
I have provided examples of unit tests and UI tests. 

### Things I learnt ###

I really enjoyed using the Combine and SwiftUI API's. The reactive style of coding seems to both reduce code and complexity and I was surprised at how quickly and easy it was to design a screen and have it hooked up to a back end. I've found the property wrappers to be really powerful in adding behaviour to variables and I can't wait to add my own property wrappers in future, replacing the "didSet" arguments which sit everywhere in my code. 

I did discover an issue with the property wrappers and how they're not currently supported in protocols without a work around. I wanted to expose the isConnected property into a NetworkMonitor Protocol so I could create a mock object for testing but I couldn't do this due to isConnected using the @Published property wrapper. I've found a couple of hacky ways around this on Stack Overflow but didn't go as far as implementing them in the tech task. My only reason for doing this was so I could dependancy inject the network monitor for ease of testing. However, I found another way around this, by setting launch arguments. I thought about refactoring the network monitor and using the hacky method for using property wrappers in a protocol but then decided against it as I've used dependancy injection elsewhere in the app and wanted to demonstrate my knowledge of launch arguments as well to show that there's numerous ways around an issue. 

Overall, I'm happy that I've had the opportunity to use Swift UI and Combine in a "commercial" sense and see how powerful the new frameworks are. Even if I'm not hired for the position, I'm thankful for the chance to learn something new and showcase my knowledge.

I look forward to the feedback and any chances to grow as a developer.
